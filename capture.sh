#!/bin/bash

TOTAL_TIME=60000
INTERVAL=1000

rm *.jpg

while [ 1 ]
do
	# Do captures 
	raspistill  -w 640 -h 480 -rot 180  -o photo%08d.jpg -t $TOTAL_TIME -tl $INTERVAL -v

	# Create a folder for moving all captures to it
	DATE=$(date +%Y%m%d%H%M%S)
	mkdir $DATE
	mv *.jpg $DATE/

	# Encode timelapse file
	./encode.sh $DATE &
done
