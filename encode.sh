
# Add timestamp to files
mkdir $1/final
cd $1
for file in *.jpg
do
	convert "$file" -font ../arial.ttf \
		-pointsize 36 -fill red -annotate +10+40  \
		%[exif:DateTimeOriginal] "final/${file}"
done

# Convert files
cd final
avconv -y -r 10 -i photo%08d.jpg -r 10 -vcodec libx264 -q:v 3  timelapse-$1.mp4

# Put file in server
smbclient //192.168.1.2/data -A ../../samba-credentials.txt -c "cd Timelapses; put timelapse-$1.mp4"

# Remove files
cd ../../
rm -rf $1

